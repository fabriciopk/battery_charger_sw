# MSP430 Makefile

#######################################
# User configuration 
#######################################
TARGET = main
# MCU: part number to build for
MCU =  msp430g2553
# SOURCES: list of input source sources
SOURCES = main.c
# OUTDIR: directory to use for output
OUTDIR = build
OBJ_DIR=$(OUTDIR)/obj
BIN_DIR=$(OUTDIR)/bin
# Sources directory
SRC_DIR=src
# INCLUDES: list of includes, by default, use Includes directory
INC_DIR=include
INCLUDES = -I$(INC_DIR)
# define flags
CFLAGS = -mmcu=$(MCU) -g -Os -c -Wall -Wunused  $(INCLUDES)
ASFLAGS = -mmcu=$(MCU) -x assembler-with-cpp -Wa,-gstabs
LDFLAGS = -mmcu=$(MCU) -Wl,-Map=$(OUTDIR)/$(TARGET).map
#######################################
# end of user configuration
#######################################
#
#######################################
# binaries
#######################################
CC      	= msp430-gcc
LD      	= msp430-ld
AR      	= msp430-ar
AS      	= msp430-gcc
GASP    	= msp430-gasp
NM      	= msp430-nm
OBJCOPY 	= msp430-objcopy
MAKETXT 	= srec_cat
UNIX2DOS	= unix2dos
RM      	= rm -f
MKDIR		= mkdir -p
MSPDEBUG = mspdebug
#######################################
# Attempt to create the output directories
ifneq ($(OUTDIR),)
$(shell [ -d $(OUTDIR) ] || mkdir -p $(OUTDIR))
$(shell [ -d $(OBJ_DIR) ] || mkdir -p $(OBJ_DIR))
$(shell [ -d $(BIN_DIR) ] || mkdir -p $(BIN_DIR))
endif

# Source files
SRCS:=$(wildcard $(SRC_DIR)/*.c)
# Object files
OBJS:=$(patsubst %.c,$(OBJ_DIR)/%.o,$(notdir $(SRCS)))
#create output file
$(BIN_DIR)/$(TARGET).out : $(OBJS)
	$(CC) $(LDFLAGS) $^ -o $@
#compile object files
$(OBJ_DIR)/%.o : $(SRC_DIR)/%.c
	$(CC) $(CFLAGS) $< -o $@
#upload to the msp board using mspdebug
upload: $(BIN_DIR)/$(TARGET).out
	@echo "\nUploading to board..."
	$(MSPDEBUG) rf2500 'erase' "prog $(BIN_DIR)/$(TARGET).out" 'exit'
#clean bin directorys
.PHONY: clean
clean:
	-$(RM) -R $(OUTDIR)/
