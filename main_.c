//------------------------------------------------------------------------//
//							Universidade Federal de Santa Maria												//
//											Centro de Tecnologia 															//
// 									Engenharia de Controle e Automação 										//
//											Fabricio Bottega																	//
//								Prof. Rafael Concatto Beltrame													//
//																																				//
//========================================================================//
//																																				//
//															Descrição do Programa											//
//																																				//
//========================================================================//
/*
*
*		Controle malha fechada de um conversor bidirecional
*		buck-boost para um carregador de baterias

**** Especificações do conversor
Po = 18 W Potência de saída
Vin = 15 V Tensão CC de entrada
Vo = 30 V Tensão CC de saída
fs = 10 kHz Frequência de chaveamento
fa = 2.5 kHZ Frequência de amostragem
fclk = 16 MHz Frequência de clock

**** Equivalencias ADC*********
Vb ADC = 1023 <-> 16V
Vequalization (14.5V) = 927 ADC
Vfluctuation (13.8V) = 892 ADC
Vcharging (13V) = 830 ADC

Ib ADC = 1023 <-> 4A
Imax charging (3A) = 767 ADC
Ib flu (change equalization to fluctuation) (180mA) = 46 ADC


 Configurações dos pinos  MSP430G2x13/G2x53
             -----------------
         /|\|              XIN|-
          | |                 |
          --|RST          XOUT|-
            |                 |
            |         P1.1/CA1|--> Vref
            |                 |

*/
#include <msp430.h>
#include "board.h"
#include "uart.h"
#define VBEQU 927
#define VBFLU 892
#define VBCHA 830
#define IBMAX 767
#define IBFLU 767
#define TPWM 1000
typedef struct {
	int Ref; 	// Reference
	int Fdb;	// Feedback
	int Err;	// Error
	int Kp;     // Proportional
	int Ki_ts;  // Integral x Sample time
	int OutMax; // Saturation Up
	int OutMin; // Saturation Down
	int Out;	// Output
	int integral; // Output integral
	int	(*compute)();
} PIREG;
typedef PIREG *PIREG_handle;
void pireg_compute(PIREG_handle);

void ADC10_ISR(void);

unsigned int samples[2]; //for holding the conversion results


PIREG pi_Vb = {927, 0, 0, 1, 1, IBMAX, 0, 0, 0, pireg_compute};
PIREG pi_Ib = {0, 0, 0, 1, 1, (TPWM-1), 1, 0, 0, pireg_compute};


void main (void){


	WDTCTL = WDTPW + WDTHOLD;                 // Stop WDT

	//Configure Timer CPU
	BCSCTL1 = CALBC1_16MHZ;
	DCOCTL = CALDCO_16MHZ;

	//Configure PWM
	//Freq PWM = Freq clock /(*x*TACCR0)// x=2 if UP-DOWN, else x=1
	TA0CTL   = TASSEL_2 + MC_3 + ID_0  ;// SMCLK, up-down mode, Timer A input divider /1
	TACCR0  = TPWM;						// PWM Period

	TA0CCTL1 = OUTMOD_2;				// Timer_A.OUT1 PWM Toggle/reset
	TACCR1  = 500;  				// PWM Duty Cycle

	TACCR2  = 1000-1;					// Period of Timer_A.OUT2 to trigger ADC
	TA0CCTL2 |= OUTMOD_6;				// Timer_A.OUT2 Toggle/Reset

	P1DIR   |= BIT2 + BIT6;				// P1.2 and P1.6 -> output
	P1SEL   |= BIT2;    				// P1.2 =  TA1 output
	P1SEL &= ~BIT6;						// P1.6 -> I/O

	//Configure ADC
	ADC10CTL0 &= ~ENC;						// Disable ADC
	ADC10CTL1 = INCH_1 + CONSEQ_3 + SHS_3 + ADC10DIV_0 + ADC10SSEL_3 + ADC10SHT_3;
	// A4 A3 - A1 A0 -> Multi channel
	// Repeat sequence of channels
	// Timer_A.Output 2 trigger ADC
	ADC10CTL0 = SREF_1 + ADC10SHT_2 + MSC+ REF2_5V+ REFON + ADC10ON + ADC10IE;
	// VR+ = VREF+ and VR- = VSS
	// 16 x ADC10CLKs, Multi channel enabled

	ADC10AE0 = BIT0 + BIT3;					// P1.0 -> A0 (sample[1] -> Ib), P1.1 -> A1 (sample[0] -> Vb)
	ADC10DTC1 = 0x02;						// 2 conversions
	ADC10SA = (int)samples;					// Buffer
	ADC10CTL0 |= ENC+ADC10SC;				// Sampling and conversion ready

	__enable_interrupt();

	for (;;)	  {

		if(samples[0] >= VBEQU){
			pi_Vb.Ref = VBFLU;
		}else if(samples[0] < VBCHA){
			pi_Vb.Ref = VBEQU;
		}
	}

}

void pireg_compute(PIREG *v){
	v->Err =  v->Ref - v->Fdb;
	v->integral =  v->integral + v->Ki_ts*v->Err;
	if ( v->integral > v->OutMax){
		v->integral = v->OutMax;
	} else if (v->integral < v->OutMin){
		v->integral =  v->OutMin;
	}

	v->Out = v->integral + v->Kp*v->Err;

	if ( v->Out > v->OutMax){
		v->Out = v->OutMax;
	} else if ( v->Out < v->OutMin){
		v->Out =  v->OutMin;
	}
}



#pragma vector=ADC10_VECTOR
__interrupt void ADC10_ISR(void){
	P1OUT |= BIT6;
	ADC10CTL0 &= ~ENC;				// Disable ADC
	P1OUT &= ~BIT6;
	pi_Vb.Fdb = (int)samples[0];
	pi_Vb.compute(&pi_Vb);
	pi_Ib.Ref = pi_Vb.Out;
	pi_Ib.Fdb = (int)samples[1];
	pi_Ib.compute(&pi_Ib);
	//TACCR1  = pi_Ib.Out;


	while (ADC10CTL1 & ADC10BUSY);		// Wait if ADC10 core is active
	ADC10SA = (int)samples;			// Data buffer start
	ADC10CTL0 |=  ENC+ADC10SC;		// Enable and start ADC
	ADC10CTL0 &= ~ADC10IFG;			// Clear interrupt

	return;
}
