
#include <stdio.h>
#include <string.h>
#include <stdint.h>


#define BUFFER_SIZE 256
volatile char UART_BUFFER[BUFFER_SIZE];
volatile unsigned int UART_INDEX;

/**
 * \brief Initialize the UART peripheral
 * \param[in] config - the UART configuration
 * \return 0 on success, -1 otherwise
 */
int uart_init();
void uart_send_buffer(const char *buf, int len);
void uart_send(const char* string);
void uart_buffer_clear();
void print(const char *s);
int putchar(int c) ;
