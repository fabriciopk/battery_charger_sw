
#ifndef __BOARD_H__
#define __BOARD_H__

/**
 * \brief Initialize all board dependant functionality
 * \return 0 on success, -1 otherwise
 */
int board_init(void);

#endif /* __BOARD_H__ */
