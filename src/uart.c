#include <msp430.h>
#include "uart.h"

/**
 * \brief Initialize the UART peripheral
 * \param[in] config - the UART configuration
 * \return 0 on success, -1 otherwise
 */
int uart_init()
{
  const unsigned long baud_rate_20_bits = (16000000 + (9600 >> 1)) / 9600; // Bit rate divisor
  /* USCI should be in reset */
  if (UCA0CTL1 & UCSWRST) {
    // Configure P1.1 and P1.2 as UART controlled pins
        // Configure USCI UART for 2400
    UCA0CTL1 = UCSWRST;                             // Hold USCI in reset to allow configuration
    UCA0CTL0 = 0;                                   // No parity, LSB first, 8 bits, one stop bit, UART (async)
    UCA0BR1 = (baud_rate_20_bits >> 12) & 0xFF;     // High byte of whole divisor
    UCA0BR0 = (baud_rate_20_bits >> 4) & 0xFF;      // Low byte of whole divisor
    UCA0MCTL = ((baud_rate_20_bits << 4) & 0xF0) | UCOS16; // Fractional divisor, over sampling mode
    UCA0CTL1 = UCSSEL_2;                            // Use SMCLK for bit rate generator, then release reset

    return 0;
  }
  return -1;
}
/**
 * \brief Clear the uart buffer
 */
void uart_buffer_clear(){
    memset((void*)UART_BUFFER, 0, BUFFER_SIZE);
    UART_INDEX = 0;
}

int putchar(int c) {
    while(!(IFG2 & UCA0TXIFG));                  // wait for TX buffer to be empty
    UCA0TXBUF = c;

    return c;
}

void print(const char *s) {
    while(*s) putchar(*s++);
}


/**
 * \brief Write a string to UART
 * \param[in] string - string to send
 */
void uart_send(const char* string){
    while (*string != '\0'){
        /* Transmit data */
        UCA0TXBUF = *string;
        /*Wait for transmit buffer to be ready*/
        while(! (UC0IFG & UCA0TXIFG));
        string++;
    }
}
/**
 * \brief Write a buffer to UART
 * \param[in] buf - array of char
 * \param[in] len - lenth of the buffer
 */
void uart_send_buffer(const char *buf, int len){
    unsigned int i;
    for (i=0; i<len; i++){
        UCA0TXBUF = buf[i];
        while(! (UC0IFG & UCA0TXIFG));
    }
}

__attribute__((interrupt(USCIAB0RX_VECTOR))) void rx_isr(void)
{
    if (IFG2 & UCA0RXIFG) {
        UART_BUFFER[UART_INDEX++] = UCA0RXBUF;

        /* Clear the interrupt flag */
        IFG2 &= ~UCA0RXIFG;

        UART_BUFFER[UART_INDEX] = '\0';
    }
}
