#include <msp430.h>
#include "board.h"
#include "uart.h"
/**
 * \brief Initialize all board dependant functionality
 * \return 0 on success, -1 otherwise
 */
#define TXD BIT2
#define RXD BIT1

int board_init(void)
{

  /* Hold the watchdog */
  WDTCTL = WDTPW + WDTHOLD;

  /* Configure the clock module - MCLK = 1MHz */
  DCOCTL = 0;
  BCSCTL1 = CALBC1_16MHZ;
  DCOCTL = CALBC1_16MHZ;

  P1SEL |= RXD + TXD ; // P1.1 = RXD, P1.2=TXD
  P1SEL2 |= RXD + TXD ; // P1.1 = RXD, P1.2=TXD

  //Configure PWM
	//Freq PWM = Freq clock /(*x*TACCR0)// x=2 if UP-DOWN, else x=1
	TA0CTL   = TASSEL_2 + MC_3 + ID_0  ;// SMCLK, up-down mode, Timer A input divider /1
	TACCR0  = 1000;						// PWM Period

	TA0CCTL1 = OUTMOD_2;				// Timer_A.OUT1 PWM Toggle/reset
	TACCR1  = 500;  				// PWM Duty Cycle

	TACCR2  = 1000-1;					// Period of Timer_A.OUT2 to trigger ADC
	TA0CCTL2 |= OUTMOD_6;				// Timer_A.OUT2 Toggle/Reset

	P1DIR   |= BIT2 + BIT6;				// P1.2 and P1.6 -> output
	P1SEL   |= BIT2;    				// P1.2 =  TA1 output
	P1SEL &= ~BIT6;						// P1.6 -> I/O


  /** Initialize the serial communication */

  if (uart_init() != 0) {
      while (1);
  }

  /** Global interrupt enable */
  __enable_interrupt();

  /** Read the watchdog interrupt flag */
  if (IFG1 & WDTIFG) {
      /* Clear if set */
      IFG1 &= ~WDTIFG;
  }

  /**
   * Enable the watchdog, watchdog timeout is set to an interval
   *  of 32768 cycles
   *   - sourced by ACLK
   *   - interval = 32786 / 12000 = 2.73s
   */
  WDTCTL = WDTPW + (WDTSSEL | WDTCNTCL);

  return 0;
}
