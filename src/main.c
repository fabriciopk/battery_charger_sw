//------------------------------------------------------------------------//
//			 Universidade Federal de Santa Maria			                       //
//			        Centro de Tecnologia                                   //
//        Egenharia de Computação                                    //
//				      Fabricio Bottega			                                //
//					Prof. Rafael Concatto Beltrame			                       //
//																	                                    //
//========================================================================//
//																		                                   //
//		                      Descrição do Programa                        //
//																		                                   //
//========================================================================//
/*

            Controle em malha fechada de um conversor bidirecional
            buck-boost para um carregador de baterias

   Etapa Buck:
   - DC_in  = 1
      - Vb > 16.4
          - Carregamento corrente constante
                - Lê corrente, tensão
                - Calcula o erro (Fdb - IB_CHARGE_REF)
                - Compensador PI
                - Ajusta razão cíclica

      - Vb < 16.4
          - Carregamento tensão constante
              - Lê corrente, tensão
                - Se corrente < 0.1, bateria carregada
              - Calcula o erro (Fdb - VB_FLUCTUATION)
              - Compensador PI
              - Ajusta razão cíclica

   Etapa Boost:

   - DC_in  = 0
      - Etapa de descarga


 **** Especificações do conversor operando como buck
   Po =   18 W Potência de saída
   Vin =  20 V Tensão CC de entrada
   Vo =   14.5 V Tensão CC de saída
   fs =   30 kHz Frequência de chaveamento
   fa =   2.5 kHZ Frequência de amostragem
   fclk = 16 MHz Frequência de clock

 **** Equivalencias ADC*********
   Vb ADC = 1023 <-> 16V
   Vequalization (14.5V) = 927 ADC
   Vfluctuation (13.8V) = 892 ADC
   Vcharging (13V) = 830 ADC

   Ib ADC = 1023 <-> 4A
   Imax charging (3A) = 767 ADC
   Ib flu (change equalization to fluctuation) (180mA) = 46 ADC


      pin configuration MSP430G2553
             -----------------
 |            TEST |--->
      Ib--->| A0         RESET|--->
     tx<--->| P1.1            |
     rx<--->| P1.2            |
      Vb--->| A3        P1.6  |--> PWM
 |                 |
   DC_in--->| A5              |

*/

#include <msp430.h>

unsigned int ADC_sampples[4];   // Array to hold ADC values: ADC[0]-->Vb ADC[3]-->Ib
int newsample = 0;     // Flag the main routine for new sample

#define EN_FEEDBACK 1     //enable the feedback controller
#define IBMAX   742        //Max allowed current
#define IBMIN   742
#define VBMAX   580          //Max allowed voltage
#define IB_CHARGE_REF 742     //charging current reference (CC state)
#define VB_FLUCTUATION 542   //charging voltage reference (CV state)
#define TMAX 800

typedef struct {
        int Ref;    	// Digital value of reference
        int Fdb;    	// Feedback
        int Err;    		// Error
        int Err_lst1;      // previous error [n-1]
        int coef1;   	 // PI coef1
        int coef2;   	 //  PI coef2
        int OutMax; 	 // Saturation Up pwm
        int OutMin;  	 // Saturation Down pwm
        int Out_lst1;     // Output
        int Out;     	// Output
        int Base;    	 // 2^(base) - fixed point
} reg_pi;


typedef reg_pi *pi;
void PI_compute(pi);

//Create a struct to hold the parameter of the Ib controler (digital PI);
static reg_pi pi_Vo = {400, 0, 0, 1, 41, -39, 2, 3, 0, 0, 4};
static reg_pi pi_IL = {188, 0, 0, 1, 28, -26, 2, 3, 0, 0, 5};


static int vo=0, iL=0;    // Entradas analógicas
static signed int uv=0, uv_lst1=0, ev=0, ev_lst1=0, uil=0, uil_lst1=0, uil_aux=0, eil=0, eil_lst1=0;
static signed int aux1=0, aux2=0, aux3=0, aux4=0, aux12=0, aux34=0;
static signed int voref=0, iLref=737;

int main(void)
{
        WDTCTL = WDTPW + WDTHOLD;       // Stop WDT

        DCOCTL = CALDCO_16MHZ;          // Set uC to run at approximately 16 Mhz
        BCSCTL1 = CALBC1_16MHZ;


        TA0CTL   = TASSEL_2 + MC_3 + ID_0;// SMCLK, up-down mode, Timer A input divider /1
        TACCR0  = 500;                  // PWM Period

        TA0CCTL1 = OUTMOD_2 + CCIE;     // Timer_A.OUT1 PWM Toggle/reset
        TACCR1  = 250;                 // PWM Duty Cycle


        TACCR2  = 500-1;                // Period of Timer_A.OUT2 to trigger ADC
        TA0CCTL2 |= OUTMOD_6  + CCIE;   // Timer_A.OUT2 Toggle/Reset

        P1DIR   |= BIT2 + BIT6;           // P1.2 and P1.6 -> output
        P1SEL   |= BIT2;                  // P1.2 =  TA1 output
        P1SEL &= ~BIT6;                   // P1.6 -> I/O

        //INCH_3: Enable A3 first, Use SMCLK, Sequence of Channels
        ADC10CTL1 = INCH_3 + ADC10SSEL_3 + CONSEQ_1 + SHS_3;
        // Turn on ADC,  Put in Multiple Sample and Conversion mode,  Enable Interrupt
        ADC10CTL0 = ADC10ON + REF2_5V + REFON + MSC + ADC10IE;

        ADC10AE0 |= BIT0 + BIT3;      // Enable A0 and A3 which are P1.0,P1.3

        ADC10DTC1 = 4;                // Four conversions.
        ADC10SA = (short)&ADC_sampples[0];     // ADC10 data transfer starting address.

        _BIS_SR(GIE); // Enable global interrupt

        while(1) {
                if (newsample) {
                        newsample = 0;
                }
        }

}


void Ci_compute(reg_pi *r){
    int aux1, aux2, aux12;

    r->Err_lst1 = r->Err;
    r->Err = r->Ref - r->Fdb;
    r->Out_lst1 = r->Out;

    aux1 = r->coef1*r->Err;

    if (aux1<-30000) {aux1=-30000;}
    if (aux1>30000) {aux1=30000;}

    aux2 = r->coef2*r->Err_lst1;

    if (aux2<-30000) {aux2=-30000;}
    if (aux2>30000) {aux2=30000;}

    aux12 = aux1 + aux2;             		//Q7*QAD
    aux12 = aux12 >> r->Base;   		//QAD

    r->Out = aux12 + r->Out_lst1;

    if(r->Out > 499)  {r->Out = TMAX - 2; }
    if(r->Out < 5) { r->Out  = 2; }
}


int PI_corrente(int iL) {

  iLref = 614;	// 1.5 A

	eil_lst1 = eil;
	eil = iLref - iL;
	uil_lst1 = uil;

	aux1 = 27*eil;				   //Q5*QAD

  if (aux1<-30000) {aux1=-30000;}
  if (aux1>30000) {aux1=30000;}

	aux2 = -26*eil_lst1;	   //Q5*QAD

  if (aux2<-30000) {aux2=-30000;}
  if (aux2>30000) {aux2=30000;}

	aux12=aux1+aux2;	       //Q5*QAD
	aux12=aux12>>4;          //Q1*QAD
	uil = aux12 + uil_lst1;	 //Q1*QAD
	uil_aux = uil>>1; 			//QAD


	//  Saturação da lei de controle
	if (uil_aux >= 500-2) { uil= 500-2; }
	if (uil_aux<= 2) { uil = 2; }

  return uil_aux;
}


// Timer A0 interrupt
#pragma vector=TIMER0_A1_VECTOR
__interrupt void Timer1_A (void)
{
        //starts a new adc sample when: max counter, reset/set
        switch( TA0IV )
        {
        case  1:
                break;
        case  4://reset/set
                P1OUT |= BIT6;
                ADC10CTL0 |= ENC + ADC10SC; // Enable Sampling and start conversion.
                P1OUT &= ~BIT6;
                break;
        }
}

//ADC10 interrupt
#pragma vector=ADC10_VECTOR
__interrupt void ADC10_ISR (void) {
        ADC10CTL0 &= ~ENC;  // Disable ADC

        #if defined EN_FEEDBACK
        //pi_Vb.Fdb = (int)ADC_sampples[0]; //get adc value for Vb
        //pi_IL.Fdb = (int)ADC_sampples[3]; //get adc value for Ib
        //Apply the controller
        //Ci_compute(&pi_IL);
        //TODO: compute new duty cycle value here? or outside in the main function???
        TACCR1  = PI_corrente((int)ADC_sampples[3]);//pi_IL.Out; //Update duty cycle
        #endif

        ADC10CTL0 &= ~ADC10IFG;        // clear interrupt flag
        ADC10SA = (short)&ADC_sampples[0]; // ADC10 data transfer starting address.
        newsample = 1;      // we got a sample
}
